const compile = require('../index')
const chai = require('chai')
const expect = require('chai').expect

chai.use(require('chai-fs'))

describe('Compile', function () {
  describe('Compile contract', function () {
    it('Should compile a contract', async function () {
      compile('Doggie.sol', './test/fixtures', './build')
      expect('./build/Doggie.json').to.be.a.file().with.contents.that.match(/bytecode/)
    })

    it('Should fail to compile this file', async function () {
      compile('index_test.js', './test', './build')
    })
  })
})
