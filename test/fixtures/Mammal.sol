pragma solidity >=0.4.21 <0.6.0;

contract Mammal {
  event Breath();

  function breathe() public {
    emit Breath();
  }
}
