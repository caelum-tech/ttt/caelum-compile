pragma solidity >=0.4.21 <0.6.0;

import "./test/fixtures/Mammal.sol";

contract Doggie is Mammal {
  event Pet();

  function pet() public {
    emit Pet();
  }
}
