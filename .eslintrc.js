module.exports = {
    'env': {
      'browser': true,
      'commonjs': true,
      'es6': true,
      'mocha': true
    },
    'extends': 'standard',
    'parserOptions': {
      'ecmaVersion': 2018
    },
    'rules': {
    }
  }
  
