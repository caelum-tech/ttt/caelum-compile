// imports & defines
const path = require('path')
const solc = require('solc')
const fs = require('fs-extra')

// Functions

/**
 * Makes sure that the build folder is deleted, before every compilation
 * @returns {*} - Path where the compiled sources should be saved.
 */
function compilingPreperations (outputDir) {
  const buildPath = path.resolve(__dirname, outputDir)
  fs.removeSync(buildPath)
  return buildPath
}

/**
 * Returns and Object describing what to compile and what need to be returned.
 */
function createConfiguration (sourceFile, sourceDir) {
  return {
    language: 'Solidity',
    sources: {
      [`${sourceFile}`]: {
        content: fs.readFileSync(path.resolve(__dirname, sourceDir, sourceFile), 'utf8')
      }
    },
    settings: {
      outputSelection: { // return everything
        '*': {
          '*': ['*']
        }
      }
    }
  }
}

/**
 * Compiles the sources, defined in the config object with solc-js.
 * @param config - Configuration object.
 * @returns {any} - Object with compiled sources and errors object.
 */
function compileSources (config) {
  try {
    return JSON.parse(solc.compile(JSON.stringify(config), getImports))
  } catch (e) {
    console.log(e)
  }
}

/**
 * Searches for dependencies in the Solidity files (import statements). All import Solidity files
 * need to be declared here.
 * @param dependency
 * @returns {*}
 */
function getImports (dependency) {
  return { contents: fs.readFileSync(path.resolve(__dirname, '.', dependency), 'utf8') }
}

/**
 * Shows when there were errors during compilation.
 * @param compiledSources
 */
function errorHandling (compiledSources) {
  if (!compiledSources) {
    return 'No output'
  } else if (compiledSources.errors) { // something went wrong.
    return compiledSources.errors.map(error => console.log(error.formattedMessage))
  } else return false
}

/**
 * Writes the contracts from the compiled sources into JSON files, which you will later be able to
 * use in combination with web3.
 * @param compiled - Object containing the compiled contracts.
 * @param buildPath - Path of the build folder.
 */
function writeOutput (compiled, buildPath) {
  fs.ensureDirSync(buildPath)

  for (let contractFileName in compiled.contracts) {
    // strip the file extension to get the class name
    var contractName = contractFileName.replace('.sol', '')

    // remove the path to the included file
    if (contractName.includes('/')) {
      contractName = contractName.substring(contractName.lastIndexOf('/') + 1)
    }

    // prepare the json to write
    let contractJson = {
      abi: compiled.contracts[contractFileName][contractName].abi,
      bytecode: '0x' + compiled.contracts[contractFileName][contractName].evm.bytecode.object
    }

    // write the file to disk
    fs.outputJsonSync(
      path.resolve(buildPath, contractName + '.json'),
      contractJson
    )
  }
}

// Workflow
function compile (sourceFile, sourceDir, outputDir) {
  const buildPath = compilingPreperations(outputDir)
  const config = createConfiguration(sourceFile, sourceDir)
  const compiled = compileSources(config)
  const error = errorHandling(compiled)
  if (error === false) {
    writeOutput(compiled, buildPath)
    return true
  } else {
    return error
  }
}

module.exports = compile
